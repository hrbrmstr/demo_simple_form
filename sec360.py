from flask import Flask, render_template, request, redirect, url_for
import prefilled
sec360 = Flask(__name__)

@sec360.route('/', methods=['GET','POST'])
def index():
    if request.method == 'GET':
        return render_template('form.html')

    if request.method == 'POST':
        if request.form['variety'] == 'laptop':
            return redirect(url_for('laptop_form'))
        if request.form['variety'] == 'misdelivery':
            return redirect(url_for('misdelivery'))
        else:
            return redirect(url_for('notyet'))

@sec360.route('/about')
def about():
    return render_template('about.html')

@sec360.route('/contact')
def contact():
    return render_template('contact.html')

@sec360.route('/notyet')
def notyet():
    return render_template('notyet.html')

@sec360.route('/laptop_form', methods=['GET','POST'])
def laptop_form():
    if request.method == 'GET':
        return render_template('laptop.html')
    else:
        return prefilled.laptop(request.form)

@sec360.route('/misdelivery', methods=['GET','POST'])
def misdelivery():
    if request.method == 'GET':
        return render_template('misdelivery.html')
    else:
        return prefilled.misdelivery(request.form)

if __name__ == "__main__":
    sec360.run(debug=True, host="0.0.0.0")

