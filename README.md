This is a simple python web application to demonstrate how to create a simplified VERIS entry form. This does not save incident data and make assumptions for a fictitious business that suffers security incidents.

Setup
=====
This app was written in python version 2.7.2 on Mac OSX. I recommend using virtualenv to keep your libraries straight. Setup the app like this

    sudo easy_install virtualenv
    virtualenv venv
    source venv/bin/activate
    pip install Flask

Running
=======
Run the app by typing

    python sec360.py

and then point a browser at http://localhost:5000
