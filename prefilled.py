import pprint
import json

def victim(params):
    """ This app is supposed to be for a single organization. That org might choose to populate
    all of their victim information using the app rather than having the incident responders
    do so."""
    vics = []
    vic1 = {}
    vic1["victim_id"] = "Bob's Bagel Shop"
    vic1["industry"] = "722513"
    vic1["employee_count"] = "1 to 10"
    vic1["country"] = "United States"
    vic1["state"] = "MN"
    vic1["revenue"] = { "amount":1000000, "iso_currency_code":"USD"}
    vic1["locations_affected"] = 1 
    vics.append(vic1)
    return vics

def laptop_incident_cost(record_count, record_type):
    record_count = int(record_count)
    laptop_replace_min = 1000
    laptop_replace_ml = 1500
    laptop_replace_max = 2000

    response_cost_min = 100
    response_cost_ml = 400
    response_cost_max = 2500

    record_cost = {}
    record_cost['Credentials'] = [.25,.5,.75]
    record_cost['Bank'] = [1,1.5,2]
    record_cost['Classified'] = [1,2,5]
    record_cost['Copyrighted'] = [.25,.75,3]
    record_cost['Medical'] = [.75,1,1.5]
    record_cost['Payment'] = [1,1.5,2]
    record_cost['Personal'] = [2,3,10]
    record_cost['Internal'] = [.25,.75,1.25]
    record_cost['System'] = [.25,.75,1.25]
    record_cost['Secrets'] = [3,10,100]
    
    if record_type == "Unknown":
        return [response_cost_min + laptop_replace_min,"Unknown","Unknown"]
    if record_type == "Other":
        return [laptop_replace_min, "Unknown","Unknown","Unknown"]
    minimum = response_cost_min + laptop_replace_min + (record_count * record_cost[record_type][0])
    ml = response_cost_ml + laptop_replace_ml + (record_count * record_cost[record_type][1])
    maximum = response_cost_max + laptop_replace_max + (record_count * record_cost[record_type][2])
    return [minimum, ml, maximum]

def misdelivery_incident_cost(record_count, record_type):
    record_count = int(record_count)

    response_cost_min = 100
    response_cost_ml = 400
    response_cost_max = 2500

    record_cost = {}
    record_cost['Credentials'] = [.25,.5,.75]
    record_cost['Bank'] = [1,1.5,2]
    record_cost['Classified'] = [1,2,5]
    record_cost['Copyrighted'] = [.25,.75,3]
    record_cost['Medical'] = [.75,1,1.5]
    record_cost['Payment'] = [1,1.5,2]
    record_cost['Personal'] = [2,3,10]
    record_cost['Internal'] = [.25,.75,1.25]
    record_cost['System'] = [.25,.75,1.25]
    record_cost['Secrets'] = [3,10,100]

    if record_type == "Unknown":
        return [response_cost_min, "Unknown", "Unknown"]
    if record_type == "Other":
        return [response_cost_min, "Unknown", "Unknown"]
    minimum = response_cost_min + (record_count * record_cost[record_type][0])
    ml = response_cost_ml + (record_count * record_cost[record_type][1])
    maximum = response_cost_max + (record_count * record_cost[record_type][2])
    return [minimum, ml, maximum]

def laptop_timeline(params):
    timeline = {}
    timeline["incident"] = {}
    timeline["incident"]["year"] = int(params['incident_date'].split('-')[0])
    timeline["incident"]["month"] = int(params['incident_date'].split('-')[1])
    timeline["incident"]["day"] = int(params['incident_date'].split('-')[2])
    timeline["investigation"] = {}
    timeline["investigation"]["year"] = int(params['incident_date'].split('-')[0])
    timeline["investigation"]["month"] = int(params['incident_date'].split('-')[1])
    timeline["investigation"]["day"] = int(params['incident_date'].split('-')[2])
    timeline["compromise"] = {}
    timeline["compromise"]["unit"] = "Minutes"
    timeline["exfiltration"] = {}
    timeline["exfiltration"]["unit"] = "Minutes"
    timeline["discovery"] = {}
    timeline["discovery"]["unit"] = "Hours"
    return timeline

def misdelivery_timeline(params):
    timeline = {}
    timeline["incident"] = {}
    timeline["incident"]["year"] = int(params['incident_date'].split('-')[0])
    timeline["incident"]["month"] = int(params['incident_date'].split('-')[1])
    timeline["incident"]["day"] = int(params['incident_date'].split('-')[2])
    timeline["investigation"] = {}
    timeline["investigation"]["year"] = int(params['incident_date'].split('-')[0])
    timeline["investigation"]["month"] = int(params['incident_date'].split('-')[1])
    timeline["investigation"]["day"] = int(params['incident_date'].split('-')[2])
    timeline["compromise"] = {}
    timeline["compromise"]["unit"] = "Minutes"
    timeline["exfiltration"] = {}
    timeline["exfiltration"]["unit"] = "Minutes"
    timeline["discovery"] = {}
    timeline["discovery"]["unit"] = "Hours"
    return timeline

def laptop(params):
    incident = {}
    incident["schema_version"] = "1.1"
    incident["incident_id"] = "2013sec360"
    incident["source_id"] = "Secure 360"
    incident["security_compromise"] = "Confirmed"
    incident["confidence"] = "High"
    incident["summary"] = "Organization lost control of unencrypted laptop containing non-public information"

    incident["victim"] = victim(params)

    incident["actor"] = {}
    incident["actor"]["external"] = {}
    incident["actor"]["external"]["motive"] = ["Financial"]
    incident["actor"]["external"]["role"] = ["Malicious"]
    incident["actor"]["external"]["variety"] = ["Unaffiliated"]
    incident["actor"]["external"]["country"] = ["United States"]

    incident["action"] = {}
    incident["action"]["physical"] = {}
    incident["action"]["physical"]["variety"] = ["Theft"]
    incident["action"]["physical"]["vector"] = ["Unknown"]
    incident["action"]["physical"]["location"] = [str(params['physical_location'])]

    incident["asset"] = {}
    incident["asset"]["personal"] = False
    incident["asset"]["hosting"] = False
    incident["asset"]["management"] =  False
    incident["asset"]["assets"] = []
    ass1 = {}
    ass1["variety"] = "U - Laptop"
    ass1["amount"] = 1 
    incident["asset"]["assets"].append(ass1)

    incident["attribute"] = {}
    incident["attribute"]["confidentiality"] = {}
    incident["attribute"]["confidentiality"]["data_disclosure"] = "Potentially"
    incident["attribute"]["confidentiality"]["data_total"] = int(params['data_count'])
    incident["attribute"]["confidentiality"]["data"] = [{"variety":str(params['data_variety']), "amount":int(params['data_count'])}]
    incident["attribute"]["confidentiality"]["state"] = ["Stored unencrypted"]

    incident["timeline"] = laptop_timeline(params)

    incident["discovery_method"] = "Int - reported by user"
    incident["control_failure"] = "Should have encrypted that HDD"
    incident["corrective_action"] = "Encrypt hard drives in the future"
    incident["cost_corrective_action"] = "Something in-between"

    incident["impact"] = {}
    incident["impact"]["overall_rating"] = "Minor"
    incident["impact"]["overall_min_amount"] = laptop_incident_cost(params['data_count'], params['data_variety'])[0]
    incident["impact"]["overall_amount"] = laptop_incident_cost(params['data_count'], params['data_variety'])[1]
    incident["impact"]["overall_max_amount"] = laptop_incident_cost(params['data_count'], params['data_variety'])[2]
    incident["impact"]["loss"] = []
    loss1 = {}
    loss1["variety"] = "Operating costs"
    loss1["rating"] = "Minor"
    loss1["min_amount"] = laptop_incident_cost(params['data_count'], params['data_variety'])[0]
    loss1["amount"] = laptop_incident_cost(params['data_count'], params['data_variety'])[1]
    loss1["max_amount"] = laptop_incident_cost(params['data_count'], params['data_variety'])[2]
    incident["impact"]["loss"].append(loss1)
    incident["impact"]["iso_currency_code"] = "USD"
    
    incident["plus"] = {}

    incident = json.dumps(incident, sort_keys=True, indent=4, separators=(',', ': '))
    incident = incident.replace(' ', '&nbsp;')
    incident = incident.replace('\n', '<br />')
    return incident

def misdelivery(params):
    incident = {}
    incident["schema_version"] = "1.1"
    incident["incident_id"] = "2013sec360"
    incident["source_id"] = "Secure 360"
    incident["security_compromise"] = "Confirmed"
    incident["confidence"] = "High"
    incident["summary"] = "Organization accidentally mailed non-public data to the wrong people."

    incident["victim"] = victim(params)

    incident["actor"] = {}
    incident["actor"]["internal"] = {}
    incident["actor"]["internal"]["motive"] = ["NA"]
    incident["actor"]["internal"]["role"] = ["Unintentional"]
    incident["actor"]["internal"]["variety"] = ["End-user"]

    incident["action"] = {}
    incident["action"]["error"] = {}
    incident["action"]["error"]["variety"] = ["Misdelivery"]
    incident["action"]["error"]["vector"] = ["Carelessness"]

    incident["asset"] = {}
    incident["asset"]["personal"] = False
    incident["asset"]["hosting"] = False
    incident["asset"]["management"] =  False
    incident["asset"]["assets"] = []
    ass1 = {}
    ass1["variety"] = "M - Documents"
    ass1["amount"] = int(params['data_count']) 
    incident["asset"]["assets"].append(ass1)

    incident["attribute"] = {}
    incident["attribute"]["confidentiality"] = {}
    incident["attribute"]["confidentiality"]["data_disclosure"] = "Yes"
    incident["attribute"]["confidentiality"]["data_total"] = int(params['data_count'])
    incident["attribute"]["confidentiality"]["data"] = [{"variety":str(params['data_variety']), "amount":int(params['data_count'])}]
    incident["attribute"]["confidentiality"]["state"] = ["Stored unencrypted"]

    incident["timeline"] = misdelivery_timeline(params)

    incident["discovery_method"] = str(params['discovery_method'])
    incident["control_failure"] = str(params['control_failure'])
    incident["corrective_action"] = "Encrypt hard drives in the future"
    incident["cost_corrective_action"] = "Something in-between"

    incident["impact"] = {}
    incident["impact"]["overall_rating"] = "Minor"
    incident["impact"]["overall_min_amount"] = misdelivery_incident_cost(params['data_count'], params['data_variety'])[0]
    incident["impact"]["overall_amount"] = misdelivery_incident_cost(params['data_count'], params['data_variety'])[1]
    incident["impact"]["overall_max_amount"] = misdelivery_incident_cost(params['data_count'], params['data_variety'])[2]
    incident["impact"]["loss"] = []
    loss1 = {}
    loss1["variety"] = "Operating costs"
    loss1["rating"] = "Minor"
    loss1["min_amount"] = misdelivery_incident_cost(params['data_count'], params['data_variety'])[0]
    loss1["amount"] = misdelivery_incident_cost(params['data_count'], params['data_variety'])[1]
    loss1["max_amount"] = misdelivery_incident_cost(params['data_count'], params['data_variety'])[2]
    incident["impact"]["loss"].append(loss1)
    incident["impact"]["iso_currency_code"] = "USD"

    incident = json.dumps(incident, sort_keys=True, indent=4, separators=(',', ': '))
    incident = incident.replace(' ', '&nbsp;')
    incident = incident.replace('\n', '<br />')
    return incident
